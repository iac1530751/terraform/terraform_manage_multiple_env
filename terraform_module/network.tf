resource "azurerm_virtual_network" "TF_vnet" {
    name                = "${var.resource_group_name}-${var.env_tag}-vnet"
    location            = var.resource_location
    resource_group_name = azurerm_resource_group.TF_rg.name
    address_space       = ["10.123.0.0/16"]

    tags = {
        environment = var.env_tag
    }
}