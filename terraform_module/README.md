# Terraform_Manage_Multiple_Env

### Terraform module directory. 
This is Terraform module directory. You can have module created on local repo as well in the remote repo. 
Here module has been created in the local repo. 

This is very simple module and provison RG and Vnet upon providing the variables. 

Below are Input Variables that needs to be provided to module - 
 
 resource_group_name = "apple"
 resource_location   = "West Europe"
 env_tag             = "production"
