# Module creates RG and Vnet 
# Below inputs are required for module - 

# Below are Input Variables that needs to be provided to module - 
# 
# resource_group_name = "apple"
# resource_location   = "West Europe"
# env_tag             = "production"


resource "azurerm_resource_group" "TF_rg" {
  name     = "${var.resource_group_name}-rg"
  location = var.resource_location

  tags = {
    environment = var.env_tag
  }

}