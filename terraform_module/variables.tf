
variable "resource_group_name" {
  type        = string
  description = "RG name in Azure"
  default     = "test"
}

variable "resource_location" {
  type        = string
  description = "RG location in Azure"
  default     = "West Europe"
}

variable "env_tag" {
  type        = string
  description = "Tags for the enviroment"
  default     = "test"
}