# Terraform Manage Multiple Env

#### Objective is to cover 2 aspects - 
- Manage multiple environments using the directory structure approach in TF 
- Using terraform modules for deploying the infra

This repo shows how multiple environments can be managed via terraform in directory structure manner rather than using ```terraform workspace```

Below repo consist of 4 directories - global, terraform_module, production, staging 
- ```global``` This directory is emtpy but this contain main.tf that will be used to deploy global resources that will be used by all environments.
- ```production``` This directory consist of the tf files that will be used to deploy production infra components.
- ```staging``` This directory consist of the tf files that will be used to deploy stating infra components.
- ```terraform_module``` This directory consist of the terraform module. This module will actually deploy the components when called upon.

### Understanding Terraform Module 
Terraform module is more like regular terraform code which is completely variablized. The TF files will be called from this directory to build the Azure components. 
Below is the block that needs to be added in ```main.tf``` to call the module - 
```
module "terraform_module" {
  source = "../terraform_module"

  # Input Variables
  resource_group_name = "my-rg"
  resource_location   = "West Europe"
  env_tag             = "my-env"
}
```

### Deploying ```production``` infra 
Below are the steps that needs to be carried out to deploy the ```production``` Infra 
> **Note -** Make sure that rigt variables are declared in main.tf before running any command. Variables are passed to the modules from main.tf

```
# cd production/
# terraform init 

# terraform plan 
# terraform apply -auto-approve

# terraform state list 
```
### Deploying ```staging``` infra 
Below are the steps that needs to be carried out to deploy the ```staging``` Infra 
> **Note -** Make sure that rigt variables are declared in main.tf before running any command. Variables are passed to the modules from main.tf

```
# cd production/
# terraform init 

# terraform plan 
# terraform apply -auto-approve

# terraform state list 
```

> **Note -** this code is fired from Azure cloud shell. You will need to add subscription details and credentails in case of running it from some system so terraform can authenticate with Azure.
